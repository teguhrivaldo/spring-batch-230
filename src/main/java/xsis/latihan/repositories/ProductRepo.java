package xsis.latihan.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import xsis.latihan.models.ProductModel;

@Repository
public interface ProductRepo extends JpaRepository<ProductModel,Long> {
		// guna interface ini untuk mengakses methiod dari JPA dan query

}
