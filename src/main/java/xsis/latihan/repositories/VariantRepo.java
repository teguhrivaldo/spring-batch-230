package xsis.latihan.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import xsis.latihan.models.VariantModel;


@Repository
public interface VariantRepo extends JpaRepository<VariantModel,Long> {
		// guna interface ini untuk mengakses methiod dari JPA dan query
}

