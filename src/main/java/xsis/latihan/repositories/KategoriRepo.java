package xsis.latihan.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import xsis.latihan.models.KategoriModel;

@Repository
public interface KategoriRepo extends JpaRepository<KategoriModel,Long> {
		// guna interface ini untuk mengakses methiod dari JPA dan query
}