package xsis.latihan.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

@Entity
@Table(name="kategori")
public class KategoriModel {
	
	
		
		@javax.persistence.Id
		@GeneratedValue(strategy =GenerationType.IDENTITY)
		private long Id;

		@Column(name="nama_kategori")
		private String Nama_kategori;

		public long getId() {
			return Id;
		}

		public void setId(long id) {
			Id = id;
		}

		public String getNama_kategori() {
			return Nama_kategori;
		}

		public void setNama_kategori(String nama_kategori) {
			Nama_kategori = nama_kategori;
		}
		
		
	

	}

