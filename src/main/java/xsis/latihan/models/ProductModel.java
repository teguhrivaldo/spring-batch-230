package xsis.latihan.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

@Entity
@Table(name="products")
public class ProductModel {
	
	@javax.persistence.Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	private long Id;

	@Column(name="name")
	private String Name;
	
	@Column(name="price")
	private double Price;
	
	@Column(name="stock")
	private Integer Stock;
	
	@Column(name="category_id")
	private Integer CategoryId;

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public double getPrice() {
		return Price;
	}

	public void setPrice(double price) {
		Price = price;
	}

	public Integer getStock() {
		return Stock;
	}

	public void setStock(Integer stock) {
		Stock = stock;
	}

	public Integer getCategoryId() {
		return CategoryId;
	}

	public void setCategoryId(Integer categoryId) {
		CategoryId = categoryId;
	}
}
