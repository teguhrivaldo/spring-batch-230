package xsis.latihan.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

@Entity
@Table(name="variant")
public class VariantModel {

	
	@javax.persistence.Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	private long Id;

	@Column(name="nama_variant")
	private String Nama_variant;

	@Column(name="category_id")
	private Long CategoryId ;

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public String getNama_variant() {
		return Nama_variant;
	}

	public void setNama_variant(String nama_variant) {
		Nama_variant = nama_variant;
	}

	public Long getCategoryId() {
		return CategoryId;
	}

	public void setCategoryId(Long categoryId) {
		CategoryId = categoryId;
	}
}
