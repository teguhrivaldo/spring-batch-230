package xsis.latihan.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import xsis.latihan.models.KategoriModel;
import xsis.latihan.models.VariantModel;
import xsis.latihan.repositories.KategoriRepo;
import xsis.latihan.repositories.VariantRepo;

@Controller
@RequestMapping(value="/variant/")
public class Variant {


	@Autowired
	private KategoriRepo ktgrrepo;
	

	@Autowired
	private VariantRepo variantrepo;
	

	@GetMapping(value="index")
	public ModelAndView kategori() {
		ModelAndView view =new ModelAndView("/variant/index");
		List<VariantModel> variant=this.variantrepo.findAll();
		view.addObject("variant",variant);
				return view;
	}
	

	@GetMapping(value="form")
	public ModelAndView form() {
		ModelAndView view = new ModelAndView("/variant/form");
		VariantModel variantmodel=new VariantModel();	
		List<KategoriModel> kategori=this.ktgrrepo.findAll();
		view.addObject("kategori",kategori);
		view.addObject("variant",variantmodel);
		return view;
	}
	@PostMapping(value= "save")
	public ModelAndView save(@ModelAttribute VariantModel variantmodel , BindingResult bindingresult) {
		if(bindingresult.hasErrors()) {
			return new ModelAndView("redirect:/variant/index");
		} else {
			this.variantrepo.save(variantmodel);
			return new ModelAndView("redirect:/variant/index");
		}
	}
	@GetMapping(value="edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView view= new ModelAndView("/variant/form");
		VariantModel variantmodel=this.variantrepo.findById(id).orElse(null);
		List<KategoriModel> kategori=this.ktgrrepo.findAll();
		view.addObject("kategori",kategori);
		view.addObject("variant",variantmodel);
		return view;
	}
	
	@GetMapping(value="delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		ModelAndView view= new ModelAndView("/variant/delete");
		VariantModel variantmodel=this.variantrepo.findById(id).orElse(null);
		view.addObject("variant",variantmodel);
		return view;
	}
		
	@PostMapping(value="remove")
	public ModelAndView remove(@ModelAttribute VariantModel variantmodel,BindingResult bindingresult) {
		if(bindingresult.hasErrors()) {
			return new ModelAndView("redirect:/variant/index");			
		}else {
			this.variantrepo.delete(variantmodel);
			return new ModelAndView("redirect:/variant/index");
		}
	}//close post
	
	
	
	
}//close class
