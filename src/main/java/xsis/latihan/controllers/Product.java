package xsis.latihan.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import xsis.latihan.models.KategoriModel;
import xsis.latihan.models.ProductModel;
import xsis.latihan.repositories.KategoriRepo;
import xsis.latihan.repositories.ProductRepo;

@Controller
@RequestMapping(value="/product/")
public class Product {
	
	@Autowired
	private ProductRepo productrepo;
	@Autowired
	private KategoriRepo ktgrrepo;
	
	@GetMapping(value="index")
	public ModelAndView Index() {
		ModelAndView view =new ModelAndView("/product/index");
		List<ProductModel> product=this.productrepo.findAll();
		view.addObject("product",product);
				return view;
	}
	
	@GetMapping(value="form")
	public ModelAndView form() {
		ModelAndView view = new ModelAndView("/product/form");
		ProductModel productmodel=new ProductModel();	
		List<KategoriModel> kategori=this.ktgrrepo.findAll();
		view.addObject("kategori",kategori);
		view.addObject("product",productmodel);
		return view;
	}
	
	@PostMapping(value= "save")
	public ModelAndView save(@ModelAttribute ProductModel productmodel , BindingResult bindingresult) {
		if(bindingresult.hasErrors()) {
			return new ModelAndView("redirect:/product/form");
		} else {
			this.productrepo.save(productmodel);
			return new ModelAndView("redirect:/product/index");
		}
	}
	
	@GetMapping(value="edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView view= new ModelAndView("/product/form");
		ProductModel productmodel=this.productrepo.findById(id).orElse(null);
		List<KategoriModel> kategori=this.ktgrrepo.findAll();
		view.addObject("kategori",kategori);
		view.addObject("product",productmodel);
		return view;
	}

	@GetMapping(value="delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		ModelAndView view= new ModelAndView("/product/delete");
		ProductModel productmodel=this.productrepo.findById(id).orElse(null);
		view.addObject("product",productmodel);
		return view;
	}
		
	@PostMapping(value="remove")
	public ModelAndView remove(@ModelAttribute ProductModel productmodel,BindingResult bindingresult) {
		if(bindingresult.hasErrors()) {
			return new ModelAndView("redirect:/product/index");			
		}else {
			this.productrepo.delete(productmodel);
			return new ModelAndView("redirect:/product/index");
		}
	}//close post
	
	

}//close class
	