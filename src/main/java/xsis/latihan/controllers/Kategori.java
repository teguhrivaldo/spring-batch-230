package xsis.latihan.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import xsis.latihan.models.KategoriModel;
import xsis.latihan.repositories.KategoriRepo;


@Controller
@RequestMapping(value="/kategori/")
public class Kategori {

	@Autowired
	private KategoriRepo ktgrrepo;

	
	@GetMapping(value="index")
	public ModelAndView kategori() {
		ModelAndView view =new ModelAndView("/kategori/index");
		List<KategoriModel> kategori=this.ktgrrepo.findAll();
		view.addObject("kategori",kategori);
				return view;
	}
	

	@GetMapping(value="form")
	public ModelAndView form() {
		ModelAndView view = new ModelAndView("/kategori/form");
		KategoriModel kategorimodel=new KategoriModel();
		view.addObject("kategori",kategorimodel);
		return view;
	}
	
	@PostMapping(value= "save")
	public ModelAndView save(@ModelAttribute KategoriModel kategorimodel , BindingResult bindingresult) {
		if(bindingresult.hasErrors()) {
			return new ModelAndView("redirect:/kategori/form");
		} else {
			this.ktgrrepo.save(kategorimodel);
			return new ModelAndView("redirect:/kategori/index");
		}
	}//close mapping 
	
	
	@GetMapping(value="edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView view= new ModelAndView("/kategori/form");
		KategoriModel kategorimodel=this.ktgrrepo.findById(id).orElse(null);
		view.addObject("kategori",kategorimodel);
		return view;
	}
	
	@GetMapping(value="delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		ModelAndView view= new ModelAndView("/kategori/delete");
		KategoriModel kategorimodel=this.ktgrrepo.findById(id).orElse(null);
		view.addObject("kategori",kategorimodel);
		return view;
	}
		
	@PostMapping(value="remove")
	public ModelAndView remove(@ModelAttribute KategoriModel kategorimodel,BindingResult bindingresult) {
		if(bindingresult.hasErrors()) {
			return new ModelAndView("redirect:/kategori/index");			
		}else {
			this.ktgrrepo.delete(kategorimodel);
			return new ModelAndView("redirect:/kategori/index");
		}
	}//close post
	

}
